package com.thb.blockchain.core.service;

import com.thb.blockchain.block.PairKey;
import com.thb.blockchain.common.exception.TrustSDKException;
import com.thb.blockchain.common.utils.TrustSDK;
import org.springframework.stereotype.Service;

/**
 * @author wuweifeng wrote on 2018/3/7.
 */
@Service
public class PairKeyService {

    /**
     * 生成公私钥对
     * @return PairKey
     * @throws TrustSDKException TrustSDKException
     */
    public PairKey generate() throws TrustSDKException {
        return TrustSDK.generatePairKey(true);
    }
}
