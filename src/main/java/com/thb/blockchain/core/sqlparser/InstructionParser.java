package com.thb.blockchain.core.sqlparser;

import com.thb.blockchain.block.InstructionBase;

/**
 * @author wuweifeng wrote on 2018/3/21.
 */
public interface InstructionParser {
    boolean parse(InstructionBase instructionBase);
}
